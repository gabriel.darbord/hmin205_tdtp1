package com.example.hmin205_td1_exo4;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends Activity {

    private Button button, button2;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        button = (Button) findViewById(R.id.mainbutton);
        //button.setOnLongClickListener();
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Message Bouton 1", Toast.LENGTH_LONG).show();
            }
        });

        button2 = (Button) findViewById(R.id.button2);
        button2.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Message Bouton 2", Toast.LENGTH_LONG).show();
                Snackbar.make(findViewById(R.id.mainlayout), "Exercice 4", Snackbar.LENGTH_INDEFINITE).show();
                return false;
            }
        });
    }
}
