package com.example.hmin205_tp1_exo3_java;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends Activity {
    private LinearLayout layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        // création des widgets
        TextView tv;
        EditText et;
        Button button;
        ViewGroup.LayoutParams tvp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ViewGroup.LayoutParams etp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // nom
        tv = new TextView(this);
        tv.setText("Nom :");
        layout.addView(tv, tvp);
        et = new EditText(this);
        layout.addView(et, etp);

        // prénom
        tv = new TextView(this);
        tv.setText("Prénom :");
        layout.addView(tv, tvp);
        et = new EditText(this);
        layout.addView(et, etp);

        // âge
        tv = new TextView(this);
        tv.setText("Âge :");
        layout.addView(tv, tvp);
        et = new EditText(this);
        et.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(et, etp);

        // domaine de compétences
        tv = new TextView(this);
        tv.setText("Domaine de compétences :");
        layout.addView(tv, tvp);
        et = new EditText(this);
        layout.addView(et, etp);

        // numéro de téléphone
        tv = new TextView(this);
        tv.setText("Numéro de téléphone :");
        layout.addView(tv, tvp);
        et = new EditText(this);
        et.setInputType(InputType.TYPE_CLASS_PHONE);
        layout.addView(et, etp);

        // bouton de validation
        button = new Button(this);
        button.setText("Valider");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Informations validées", Toast.LENGTH_LONG).show();
            }
        });
        layout.addView(button);

        // affichage
        this.setContentView(layout);
    }
}
