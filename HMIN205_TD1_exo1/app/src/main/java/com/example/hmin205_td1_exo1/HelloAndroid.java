package com.example.hmin205_td1_exo1;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class HelloAndroid extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // en créant les widgets programmatiquement :
        /**/
        TextView tv = new TextView(this);
        tv.setText("Hello, Android");
        setContentView(tv);

        EditText et = new EditText(this);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        addContentView(et, lp);
        /**/

        // en utilisant un fichier de layout :
        //setContentView(R.layout.layout_file);
    }
}
